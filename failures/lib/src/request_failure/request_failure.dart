import 'dart:io';

import 'package:failures/failures.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'request_failure.freezed.dart';

@freezed
class RequestFailure extends Failure with _$RequestFailure {
  const factory RequestFailure({
    required String message,
    @Default(HttpStatus.badRequest) int statusCode,
  }) = _RequestFailure;
}
