import 'dart:io';

import 'package:failures/failures.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'server_failure.freezed.dart';

/// [ServerFailure] class will be used to represent errors that occur on the server side of our application
@freezed
class ServerFailure extends Failure with _$ServerFailure {
  const factory ServerFailure({
    required String message,
    @Default(HttpStatus.internalServerError) int statusCode,
  }) = _ServerFailure;
}
