abstract class Failure {
  String get message;
  int get statusCode;
}
