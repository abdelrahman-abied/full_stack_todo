import 'package:exceptions/exceptions.dart';
import 'package:failures/failures.dart';
// import 'package:fpdart/fpdart.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:either_dart/either.dart';

part 'create_todo_dto.freezed.dart';
part 'create_todo_dto.g.dart';

@freezed
class CreateTodoDto with _$CreateTodoDto {
  factory CreateTodoDto({
    required String title,
    required String description,
  }) = _CreateTodoDto;

  factory CreateTodoDto.fromJson(Map<String, dynamic> json) =>
      _$CreateTodoDtoFromJson(json);
  static Either<ValidationFailure, CreateTodoDto> validated(
    Map<String, dynamic> json,
  ) {
    try {
      final errors = <String, List<String>>{};
      if (json['title'] == null) {
        errors['title'] = ['Title is required'];
      }
      if (json['description'] == null) {
        errors['description'] = ['Description is required'];
      }
      if (errors.isEmpty) return Right(CreateTodoDto.fromJson(json));
      throw BadRequestException(
        message: 'Validation failed',
        errors: errors,
      );
    } on BadRequestException catch (e) {
      return Left(
        ValidationFailure(
          message: e.message,
          errors: e.errors,
          statusCode: e.statusCode,
        ),
      );
    }
  }
}
