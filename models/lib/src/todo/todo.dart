import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:typedefs/typedefs.dart';

import '../serializers/date_time_serializer.dart';
part 'todo.freezed.dart';

part 'todo.g.dart';

@freezed
class Todo with _$Todo {
  factory Todo({
    required TodoId id,
    required String title,
    @Default('') String description,
    @Default(false) bool? completed,
    @DateTimeConverter() required DateTime createdAt,
    @DateTimeConverterNullable() DateTime? updatedAt,
  }) = _Todo;

  factory Todo.fromJson(Map<String, dynamic> json) => _$TodoFromJson(json);
}
