import 'dart:developer';

import 'package:dotenv/dotenv.dart';
import 'package:postgres/postgres.dart';

class DatabaseConnnection {
  DatabaseConnnection(this._dotenv) {
    _host = _dotenv['DB_HOST'] ?? 'localhost';
    _database = _dotenv['DB_DATABASE'] ?? 'test';
    _userName = _dotenv['DB_USERNAME'] ?? 'test';
    _password = _dotenv['DB_PASSWORD'] ?? 'test';
    _port = int.tryParse(_dotenv['DB_PORT'] ?? '') ?? 5432;
  }
  late final DotEnv _dotenv;
  late final String _host;
  late final int _port;
  late final String _database;
  late final String _userName;
  late final String _password;

  PostgreSQLConnection? _connection;
  PostgreSQLConnection get db =>
      _connection ??= throw Exception('Database connection not initialize');
  Future<void> connect() async {
    try {
      _connection = PostgreSQLConnection(
        _host,
        _port,
        _database,
        username: _userName,
        password: _password,
      );
      log('Database connection successful');
      return _connection!.open();
    } catch (e) {
      log('Database connection failed: $e');
    }
  }

  Future<void> close() => _connection!.close();
}
