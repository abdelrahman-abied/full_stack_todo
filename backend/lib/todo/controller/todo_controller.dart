import 'dart:io';

import 'package:dart_frog/src/_internal.dart';
import 'package:models/models.dart';
import 'package:repository/repository.dart';
import 'package:typedefs/typedefs.dart';
import 'package:either_dart/either.dart';

import 'dart:async';

import 'http_controller.dart';

class TodoController extends HttpController {
  TodoController(this._repo);

  final TodoRepository _repo;

  @override
  FutureOr<Response> index(Request request) async {
    final result = await _repo.getTodos();

    return result.fold(
      (failure) => Response.json(
        body: {'message': failure.message},
        statusCode: failure.statusCode,
      ),
      (todos) =>
          Response.json(body: todos.map((todo) => todo.toJson()).toList()),
    );
  }

  @override
  FutureOr<Response> show(Request request, String id) async {
    final todoId = mapTodoId(id);
    if (todoId.isLeft) {
      return Response.json(
        body: {'message': todoId.left.message},
        statusCode: todoId.left.statusCode,
      );
    }
    final res = await _repo.getTodoById(todoId.right);
    return res.fold(
      (left) => Response.json(
        body: {'message': left.message},
        statusCode: left.statusCode,
      ),
      (right) => Response.json(
        body: right.toJson(),
      ),
    );
  }

  @override
  FutureOr<Response> store(Request request) async {
    final parsedBody = await parseJson(request);
    if (parsedBody.isLeft) {
      return Response.json(
        body: {'message': parsedBody.left.message},
        statusCode: parsedBody.left.statusCode,
      );
    }
    final json = parsedBody.right;
    final createTodoDto = CreateTodoDto.validated(json);
    if (createTodoDto.isLeft) {
      return Response.json(
        body: {
          'message': createTodoDto.left.message,
          'errors': createTodoDto.left.errors,
        },
        statusCode: createTodoDto.left.statusCode,
      );
    }
    final res = await _repo.createTodo(createTodoDto.right);
    return res.fold(
      (left) => Response.json(
        body: {'message': left.message},
        statusCode: left.statusCode,
      ),
      (right) => Response.json(
        body: right.toJson(),
        statusCode: HttpStatus.created,
      ),
    );
  }

  @override
  FutureOr<Response> update(Request request, String id) async {
    final parsedBody = await parseJson(request);
    final todoId = mapTodoId(id);
    if (todoId.isLeft) {
      return Response.json(
        body: {'message': todoId.left.message},
        statusCode: todoId.left.statusCode,
      );
    }
    if (parsedBody.isLeft) {
      return Response.json(
        body: {'message': parsedBody.left.message},
        statusCode: parsedBody.left.statusCode,
      );
    }

    final json = parsedBody.right;
    final updateTodoDto = UpdateTodoDto.validated(json);
    if (updateTodoDto.isLeft) {
      return Response.json(
        body: {
          'message': updateTodoDto.left.message,
          'errors': updateTodoDto.left.errors,
        },
        statusCode: updateTodoDto.left.statusCode,
      );
    }
    final res = await _repo.updateTodo(
      id: todoId.right,
      updateTodoDto: updateTodoDto.right,
    );
    return res.fold(
      (left) => Response.json(
        body: {'message': left.message},
        statusCode: left.statusCode,
      ),
      (right) => Response.json(
        body: right.toJson(),
      ),
    );
  }

  @override
  FutureOr<Response> destroy(Request request, String id) async {
    final todoId = mapTodoId(id);
    if (todoId.isLeft) {
      return Response.json(
        body: {'message': todoId.left.message},
        statusCode: todoId.left.statusCode,
      );
    }
    final res = await _repo.deleteTodo(todoId.right);
    return res.fold(
      (left) => Response.json(
        body: {'message': left.message},
        statusCode: left.statusCode,
      ),
      (right) => Response.json(body: {'message': 'OK'}),
    );
  }
}
