import 'dart:async';
import 'dart:convert';
import 'package:either_dart/either.dart';

import 'package:dart_frog/dart_frog.dart';
import 'package:exceptions/exceptions.dart';
import 'package:failures/failures.dart';
// import 'package:fpdart/fpdart.dart';

abstract class HttpController {
  FutureOr<Response> index(Request request);

  FutureOr<Response> store(Request request);

  FutureOr<Response> show(Request request, String id);

  FutureOr<Response> update(Request request, String id);

  FutureOr<Response> destroy(Request request, String id);

  Future<Either<Failure, Map<String, dynamic>>> parseJson(
    Request request,
  ) async {
    try {
      final body = await request.body();
      print(body);
      if (body.isEmpty) {
        throw const BadRequestException(message: 'Invalid body');
      }

      try {
        late final Map<String, dynamic> json;

        json = jsonDecode(body) as Map<String, dynamic>;

        return Right(json);
      } catch (e) {
        throw const BadRequestException(message: 'Invalid Body');
      }
    } on BadRequestException catch (e) {
      print("$e");
      return Left(
        ValidationFailure(
          message: e.message,
          errors: {},
        ),
      );
    }
  }
}
