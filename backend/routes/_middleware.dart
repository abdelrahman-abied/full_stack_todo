import 'package:backend/db/database_connection.dart';
import 'package:backend/todo/controller/todo_controller.dart';
import 'package:backend/todo/data_source/todo_datasource_impl.dart';
import 'package:backend/todo/repositories/todo_repository_impl.dart';
import 'package:dart_frog/dart_frog.dart';
import 'package:data_source/data_source.dart';
import 'package:dotenv/dotenv.dart';
import 'package:repository/repository.dart';

Handler middleware(Handler handler) {
  final env = DotEnv()..load();
  final db = DatabaseConnnection(env);
  final todoDataSource = TodoDataSourceImpl(db);
  final todoRepo = TodoRepositoryImpl(todoDataSource);
  final todoController = TodoController(todoRepo);
  return handler
      .use(requestLogger())
      .use(provider<DatabaseConnnection>((context) => db))
      .use(provider<TodoDataSource>((context) => todoDataSource))
      .use(provider<TodoRepository>((context) => todoRepo))
      .use(provider<TodoController>((context) => todoController));
}
